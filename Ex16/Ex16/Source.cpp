#include "sqlite3.h"
#include <string>
#include <stdlib.h>
#include <iostream>

using namespace std;

const char* sql;
int rc;

void A();
void B();
static int callback(void *NotUsed, int argc, char **argv, char **azColName);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int main()
{

	B();
	system("pause");
	return 0;
}

string lastResult;

void A()
{
	sqlite3 *db;
	int rc;
	rc = sqlite3_open("FirstPart.db", &db);

	const char* sql = "CREATE TABLE People(id INT AUTO_INCREMENT, name VARCHAR(20));";
	rc = sqlite3_exec(db, sql, NULL, NULL, NULL);

	const char* sql1 = "INSERT INTO People(id, name) VALUES(1, 'Dor'),(2, 'Doria'),(3, 'Daniel')";
	rc = sqlite3_exec(db, sql1, NULL, NULL, NULL);

	sqlite3_close(db);
}
char *zErrMsg = 0;
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	try{
		string sql = "SELECT balance FROM accounts WHERE id=" + to_string(buyerid) + ";";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);

		int balance = stoi(lastResult);


		sql = "SELECT available FROM cars WHERE id=" + to_string(carid) + ";";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		int available = stoi(lastResult);

		sql = "SELECT price FROM cars WHERE id=" + to_string(carid) + ";";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		int price = stoi(lastResult);


		if (available == 1 && price <= balance)
		{
			sql = "UPDATE accounts SET balance=" + to_string(balance - price) + " WHERE id=" + to_string(buyerid) + ";";
			rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);

			sql = "UPDATE cars SET available=0 WHERE id=" + to_string(carid) + ";";
			rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
			return true;
		}
		return false;
	}
	catch (...)
	{
		return false;
	}
}

void B()
{
	sqlite3 *db;
	rc = sqlite3_open("carsDealer.db", &db);

	
	if (carPurchase(1, 1, db, NULL))
	{
		printf("Yay you bought some shitty car!");
	}
	else
	{
		printf("You don't have enough money for it Morty!");
	}

	if (carPurchase(12, 21, db, NULL))
	{
		printf("Yay you bought some shitty car!");
	}
	else
	{
		printf("You don't have enough money for it Morty!");
	}

	if (carPurchase(11, 22, db, NULL))
	{
		printf("Yay you bought some shitty car!");
	}
	else
	{
		printf("You don't have enough money for it Morty!");
	}

	printf("\n");

	balanceTransfer(1,2,1, db, zErrMsg);

	sqlite3_close(db);
}


static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	    int i;
	    for (i = 0; i<argc; i++){
		      //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
			  lastResult = argv[i] ? argv[i] : "NULL";
	}
	    printf("\n");
	    return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	try {
		sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
		string sql = "SELECT balance FROM accounts WHERE id=" + to_string(from) + ";";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		if (amount <= stoi(lastResult))
		{
			sql = "UPDATE accounts SET balance=balance+" + to_string(amount) + " WHERE id=" + to_string(to) + ";";
			rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
			sql = "UPDATE accounts SET balance=balance-" + to_string(amount) + " WHERE id=" + to_string(from) + ";";
			rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
			sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL);
			return true;
		}
		sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL);
		return false;
	}
	catch (...)
	{
		return false;
	}
}